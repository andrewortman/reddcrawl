#!/bin/bash

cd /tmp
mongodump -d reddcrawl
dump_archive_name="reddcrawl_`date +%Y.%m.%d`.tar.gz"
tar -czvf $dump_archive_name dump
s3cmd put --acl-public $dump_archive_name s3://reddcrawl_archive
rm -rf dump
rm -rf $dump_archive_name
