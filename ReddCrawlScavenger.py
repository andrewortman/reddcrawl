'''
REDDCRAWL SCAVENGER
--------------------
The ReddCrawl Scavenger actively seeks for new and rising stories that are less than
an hour old. Thats all it does. If it doesnt find any new stories, it waits a bit
before querying again. This is to alleviate some pain from the updater during
heavy news traffic.

Created on July 22, 2012

@author: andrewortman
'''
from reddcrawl import Crawler;
from reddcrawl import DatabaseManager;
import logging;
import sys;
import time;
import ConfigParser;


config = ConfigParser.RawConfigParser();
config.read("reddcrawl.conf");

''' Set up logging for reddcrawl module '''
logging.basicConfig(level=logging.INFO, format=('%(filename)s: '  
                                                 '%(levelname)s: '
                                                 '%(funcName)s(): '
                                                 '%(lineno)d:\t'
                                                 '%(message)s')
                                                );

redd_crawl = Crawler.Crawler(bot_name="reddcrawl-scavenger by /u/invalidfunction");

try:
    redd_dbm = DatabaseManager.DatabaseManager();
except Exception as e:
    logging.log(logging.CRITICAL, "Could not connect to database! Error: " + str(e));
    sys.exit(1);

while True:
    redd_dbm.UpdateScavengerHeartbeat();
    
    story_listing = redd_crawl.GetStoryListing(count=config.getint("reddcrawl", "scavenger-hourly-hotness-count"), limit=config.getint("reddcrawl", "scavenger-hourly-hotness-limit"));
    time_since_story_listing = time.time();
    
    if(story_listing == None):
        logging.error("Could not get story listing. Reddit down? Sleeping for 30 seconds..");
        time.sleep(30); #Sleep a bit before trying again.;
        continue;
    
    for story_id, story in story_listing.iteritems():
        db_story = redd_dbm.GetStory(story_id);
        if(db_story == None):
            logging.info("New story found [" + story_id + "] '" + story.title + "' - requesting more info..");
            
            #Get karma info for author
            author_karma_link, author_karma_comment = redd_crawl.GetKarmaCountsForUser(story.author_name);
            if author_karma_link == None:
                break;
            logging.info("Karma counts for " + story.author_name + " - Link: " + str(author_karma_link) + " / Comment: " + str(author_karma_comment));
            story.author_karma_link = author_karma_link;
            story.author_karma_comment = author_karma_comment;
            
            #Get subreddit subscriber conts
            subreddit_subs = redd_crawl.GetSubredditSubscriberCount(story.subreddit_name);
            if subreddit_subs == None:
                break;
            logging.info("Subscriber count for subreddit '" + story.subreddit_name + "' : " + str(subreddit_subs));
            story.subreddit_subscribers = subreddit_subs;
            
            #Finally, get duplicate information
            num_dups, dups_karma = redd_crawl.GetDuplicateInfoForStory(story._id);
            if num_dups == None:
                break;
            logging.info("Story duplicates: " + str(num_dups) + "; Total karma: " + str(dups_karma));
            story.duplicates = num_dups;
            story.duplicates_karma_total = dups_karma;
            redd_dbm.AddNewStory(story);
        else:
            #Story exists, generate a new history and append it
            #If it has reached past the 5 minute mark on the last update
            
            if (time.time() - db_story.last_update_time) > config.getint("reddcrawl", "update-interval"):
                logging.debug("Adding new history from story listing to story '" + story._id + "'");
                redd_dbm.AddHistoryToStory(story._id, story.history[0]);
            else:
                logging.debug("No need to update history for story " + str(story._id));
    
    #Lisitngs only update once per 30 seconds, so wait until we hit that
    #mark beore going again.
    processed_time = time.time() - time_since_story_listing;
    cache_time = config.getint("reddcrawl", "scavenger-cache-time");
    if processed_time < cache_time:
        logging.info("Waiting " + str(cache_time - processed_time) + " seconds before next request");
        time.sleep(cache_time - processed_time);                                          
