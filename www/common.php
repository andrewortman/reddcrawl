<?php

function get_time_string($timestamp)
{
	$time_difference = time() - $timestamp;
	if($time_difference < 3600)
	{
		$minutes = floor($time_difference/60);
		if($minutes < 5)
		{
			if(round($time_difference) <= 0)
			{
			    return "just now";
			}
			else
			{
			    return round($time_difference) . " second" . (round($time_difference) == 1 ? "" : "s") ." ago";
			}
		}
		else
		{
			if($minutes == 1)
			{
				return "1 minute ago";
			}
			else
			{
				return $minutes . " minutes ago";
			}
		}
	}
	else
	{
		$hour = floor($time_difference/3600);
		if($hour == 1)
		{
			return "1 hour ago";
		}
		else
		{
			return floor($time_difference/3600). " hours ago";
		}
	}
}

?>
