<?php
include("common.php");

header("Content-type: text/html; charset=UTF-8");
header("Cache-control: no-cache");
header("X-Powered-by: Python/PHP/MongoDB and a bit of magic");
?>

<html>
<head>
<title>REDDCrawl REALTime</title>


<!-- This is for Google Analytics. I'm not tracking you down, promise! -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-32945272-1']);
  _gaq.push(['_setDomainName', 'crawl.reddbot.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- This is for the charting api from Google -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- Adds jQuery support -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>

<!-- This handles all our charting methods -->
<script type="text/javascript">
	function updatePage() {
		$.ajax({
			url: <?php echo "\"http://$_SERVER[SERVER_NAME]/api/stats.php\""; ?>,
			success: function(data) {
				var result = jQuery.parseJSON(data);
				if(result.error == true) return;
				
				$("#num_stories").html(result.story_count);
				
				if(result.heartbeat_scavenger == true)
				{
					$("#heartbeat_scavenger").html("<span class='good'>" + result.timestring_scavenger + "</span>");
				}
				else
				{
					$("#heartbeat_scavenger").html("<span class='bad'>" + result.timestring_scavenger + "</span>");
				}
				
				if(result.heartbeat_updater == true)
				{
					$("#heartbeat_updater").html("<span class='good'>" + result.timestring_updater + "</span>");
				}
				else
				{
					$("#heartbeat_updater").html("<span class='bad'>" + result.timestring_updater + "</span>");
				}
				
				$("#timestamp").html(result.timestamp);	
			}
		});
	
	}
	$(document).ready(function(){
	
		updatePage();
				window.setInterval(updatePage, <?=(isset($_GET["interval"]) ? intval($_GET["interval"]) : 2000); ?>);
		
	});
</script>

<!-- Stops numbers from becoming phone links on mobile devices -->
<meta name="format-detection" content="telephone=no" />

<link href='http://fonts.googleapis.com/css?family=Quattrocento:700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:700' rel='stylesheet' type='text/css'>

<style type="text/css">
	
	.main_header
	{
		font-family: 'Arial', sans-serif;
		font-weight: 700;
		font-size: 20pt;
		color: white;
	}
	
	.header 
	{
		font-family: 'Quattrocento', serif;
		border-bottom:1px solid black;
		color: black;
		padding-bottom:5px;
		font-size: 16pt;
		font-weight: 700;
		font-variant:small-caps; 
		text-align: left;
	}

	#num_stories
	{
		font-family: 'Open Sans', sans-serif;
		font-weight: 700;
		font-size: 72pt;
	}
	
	.bad
	{
		color: red;
	}
	
	.good
	{
		color: green;
	}
	
	.service
	{
		font-family: 'Open Sans', sans-serif;
		font-weight: 700;
		font-size: 14pt;
		text-align: right;
	}
	
	.timestamp
	{
		font-family: 'Arial', sans-serif;
		color: #CCCCCC;
		text-transform:capitalize
		font-size:10pt;
	}
	
</style>
</head>
<body bgcolor="#111111">
<center>
<div style="display: table; height: 100%">
<div style="display: table-cell; vertical-align: middle">

<div class="main_header">REDDBOT REALTIME</div>
<div style="background-color: #DADADA; border:1px solid black; padding:5%; margin: 0 auto;">

	<div class="header">Number of Stories</div>
	<div id="num_stories">Loading..</div>
	
	<div class="header">Last Heartbeat</div>
	
	<table border='0' width='100%'>
	<tr class="service"><td style="text-align: left">Scavenger:</td><td><span id="heartbeat_scavenger">?</span></td></tr>
	<tr class="service"><td style="text-align: left">Updater:</td><td> <span id="heartbeat_updater">?</span></td></tr>
	</table>
	
</div>
<center>
	<div class="timestamp">Last updated <span id="timestamp">never.</span></div>
</center>
</div>
</div>
</center>
</body>
</html>
