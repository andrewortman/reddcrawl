<?php
header("Cache-control: no-cache");

require("../common.php");

//GZIP compression rocks, yo.
ob_start("ob_gzhandler");


try
{
	//Set up mongodb
	$mongo = new Mongo('db.crawl.reddbot.com');
	$db = $mongo->reddcrawl;

	//all times in UTC
	date_default_timezone_set("UTC"); 

	$story_count = number_format($db->stories->count());

	$last_heartbeat = $db->updater_heartbeat->findOne(array("_id" => 0));
	$updater_heartbeat_timestring = get_time_string($last_heartbeat["time"]);
	$good_heartbeat_updater = false;
	if(isset($last_heartbeat["time"]))
	{
		if($last_heartbeat["time"] > (time() - (60*10)))
		{
			$good_heartbeat_updater = true;
		}
	}

	$last_heartbeat = $db->scavenger_heartbeat->findOne(array("_id" => 0));
	$good_heartbeat_scavenger = false;
	$scavenger_heartbeat_timestring = get_time_string($last_heartbeat["time"]);
	if(isset($last_heartbeat["time"]))
	{
		    if($last_heartbeat["time"] > (time() - (60*15)))
		    {
		            $good_heartbeat_scavenger = true;
		    }
	}
	
	date_default_timezone_set("America/Chicago"); 
	echo json_encode(array("error"=>false, "story_count"=>$story_count, "heartbeat_scavenger"=>$good_heartbeat_scavenger, "timestring_scavenger"=>$scavenger_heartbeat_timestring, "heartbeat_updater"=>$good_heartbeat_updater, "timestring_updater"=> $updater_heartbeat_timestring, "timestamp"=>date("r")));
}
catch (Exception $e)
{
	echo json_encode(array("error"=>true, "error_string"=>$e->getMessage()));
	exit;
}


?>

