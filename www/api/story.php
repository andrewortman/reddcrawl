<?php
header("Cache-control: no-cache");

//GZIP compression rocks, yo.
ob_start("ob_gzhandler");

function calculate_hotness($score, $created_utc)
{
	$y = 0;
	if($score > 0) $y = 1;
	else if($score < 0) $y = -1;
	
	$z = 1;
	if(abs($score) >= 1)
	{
		$z = abs($score);
	}
	
	$ts = $created_utc - 1134028003;
	$hotness = log10($z) + ($y * $ts/45000);
	return $hotness;
}

try
{

	if(!isset($_GET["id"]))
	{
		throw new Exception("No id specified");
	}
	//Set up mongodb
	$mongo = new Mongo('db.crawl.reddbot.com');
	$db = $mongo->reddcrawl;

	//all times in UTC
	date_default_timezone_set("UTC"); 

	$story = $db->stories->findOne(array("_id" => $_GET["id"]));


	if(!isset($story["history"]))
	{
		throw new Exception("No history for this story");
	}


	//$beginning_history = array("score" => 1, "comments" => 0, "time"=>$story["created_utc"]);

	//array_unshift($story["history"], $beginning_history);
	foreach($story["history"] as &$history)
	{
		$history["hotness"] = calculate_hotness($history["s"], $story["created_utc"]);
	}

	$to_return["story"] = $story;

	//now get the frontpage history for the start/stop time range 
	$min_time_range = $story["created_utc"] - 60;
	$max_time_range = $story["history"][count($story["history"])-1]["t"] + 60;
	$histories = $db->front_page_hotness->
				find(array( "time" => array('$gt' => $min_time_range, '$lt'=>$max_time_range)));

	$frontpage_history = array();
	foreach($histories as $history)
	{
		unset($history["_id"]);
		$frontpage_history[] = $history;
	}

	$to_return["frontpage_history"] = $frontpage_history;

	echo json_encode(array_merge(array("error"=>false), $to_return));
}
catch (Exception $e)
{
	echo json_encode(array("error"=>true, "error_string"=>$e->getMessage()));
	exit;
}
?>

