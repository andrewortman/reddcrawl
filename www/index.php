<?php
include("common.php");

header("Content-type: text/html; charset=UTF-8");
header("Cache-control: no-cache");
header("X-Powered-by: Python/PHP/MongoDB and a bit of magic");

//GZIP compression rocks, yo.
ob_start("ob_gzhandler");

$perpage = 20;

if(!isset($_GET["p"]) || $_GET["p"] < 1)
{
	$pageno = 1;
}
else
{
	$pageno = $_GET["p"];
}

if(isset($_GET["pp"]))
{
	$perpage = intval($_GET["pp"]);
	if($perpage <= 0) $perpage = 25;
	else if($perpage > 150) $perpage = 150;
}

//Set up mongodb
$mongo = new Mongo('db.crawl.reddbot.com', 27017);
$db = $mongo->reddcrawl;


//all times in UTC
date_default_timezone_set("UTC"); 

//Load up default subreddits;
$default_subreddits = array();
$fp = fopen("../reddcrawl/default_subreddits.txt", "r");
while(($line = fgets($fp)))
{
    $default_subreddits[] = trim($line);
}

fclose($fp);


$current_frontpage_info = $db->front_page_hotness->find()->sort(array("time"=>-1))->limit(1);
if($current_frontpage_info->count() == 0)
{
    $fp_min = 0;
    $fp_max = 0;
}
else
{
    $current_frontpage_info = $current_frontpage_info->getNext();
    $fp_min = $current_frontpage_info["min"];
    $fp_max = $current_frontpage_info["max"];
}


function print_pagination($total_stories, $per_page, $page_no)
{
	$totalpages = ceil($total_stories/$per_page);
	if($page_no > 1)
	{
		echo "<a href=\"index.php?p=".($page_no-1)."&pp=$per_page\" rel='nofollow'>&lt;&nbsp;PREV</a>";
	}
	
	echo "&nbsp;Page $page_no of $totalpages&nbsp;";
	if($page_no < $totalpages)
	{
		echo "<a href=\"index.php?p=".($page_no+1)."&pp=$per_page\" rel='nofollow'>NEXT&nbsp;&gt;</a>"; 
	}
}

function get_number_of_stories()
{
	global $db;
	return $db->stories->count();
}

?>

<html>
<head>
<title>REDDcrawl</title>
<link rel="stylesheet" type="text/css" href="reddcrawl.css"/>

<!-- This is for Google Analytics. I'm not tracking you down, promise! -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-32945272-1']);
  _gaq.push(['_setDomainName', 'crawl.reddbot.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- This is for the charting api from Google -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- Adds jQuery support -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>

<!-- This handles all our charting methods -->
<script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
	$(document).ready(function(){

		$(".story_wrapper").each(function(index){
			var storyId = $(this).attr('id');
			$("#" + storyId + " .score").click(function()
			{
				$.ajax({
					url: <?php echo "\"http://$_SERVER[SERVER_NAME]/api/story.php?id=\"";?> + storyId,
					success: function(dataIn)
					{
						dataIn = jQuery.parseJSON(dataIn);

						$("#" + storyId + " .story_details").slideToggle();
						var dataArr = [['Minutes since creation', 'Hotness Values']];
						var dataLen = dataIn.story.history.length;
						for(var i = 0; i < dataLen; i++)
						{
							minutes_since = Math.round((dataIn.story.history[i].t - dataIn.story.created_utc) / 60);
							dataArr[i+1] = [minutes_since, dataIn.story.history[i].hotness];
						}

						var data = google.visualization.arrayToDataTable(dataArr);

				        var options = {
				          titlePosition: 'in',
				          hAxis: {title: 'Minutes since creation', viewWindowMode: 'maximized'},
				          legend: 'bottom',
				          lineWidth: 2,
				          curveType: 'function',
				          pointSize: 2,
				          colors: ['#FF4500'],
				          chartArea: {left: 50, top: 10, width: 340, height: 240},
				          width: 400,
				          height: 300
				          

				        };

				        var chart = new google.visualization.AreaChart(document.getElementById(storyId + "_hotness_graph"));
				        chart.draw(data, options);



				        var dataArr = [['Time', 'Comments', 'Score',]];
						var dataLen = dataIn.story.history.length;
						for(var i = 0; i < dataLen; i++)
						{
							minutes_since = Math.round((dataIn.story.history[i].t - dataIn.story.created_utc) / 60);
							dataArr[i+1] = [minutes_since, dataIn.story.history[i].c, dataIn.story.history[i].s];
						}
						//dataArr[dataLen+1] = dataArr[dataLen]
						var data = google.visualization.arrayToDataTable(dataArr);

				        var options = {
				          hAxis: {title: 'Minutes since creation', viewWindowMode: 'maximized'},
				          legend: 'bottom',
				          lineWidth: 2,
				          curveType: 'function',
				          tooltip: {trigger: 'none'},
				          pointSize: 2,
				          colors: ['#FF4500', '#9494FF'],
				          chartArea: {left: 50, top: 10, width: 340, height: 240},
				          width: 400,
				          height: 300
				        };

				        var chart = new google.visualization.AreaChart(document.getElementById(storyId + "_comment_score_graph"));
				        chart.draw(data, options);
				        
				        
					}
				});
			});

			$("#" + storyId + " .story_details").hide();

		});

	});
</script>

<!-- Stops numbers from becoming phone links on mobile devices -->
<meta name="format-detection" content="telephone=no" />

</head>
<body>
<a href="http://crawl.reddbot.com" style="text-decoration:none; border:0px">
<div id="topheader">

<div id="logo">
<b>REDD</b>crawl
</div>

<div id="crawlstats">
<?

	echo "<b>Crawled " . number_format($db->stories->count()) . " stories.</b><br/>";

	$last_heartbeat = $db->updater_heartbeat->findOne(array("_id" => 0));
	$good_heartbeat_updater = false;
	if(isset($last_heartbeat["time"]))
	{
		if($last_heartbeat["time"] > (time() - (60*10)))
		{
			$good_heartbeat_updater = true;
		}
	
	}

	$last_heartbeat = $db->scavenger_heartbeat->findOne(array("_id" => 0));
	$good_heartbeat_scavenger = false;
	if(isset($last_heartbeat["time"]))
	{
		    if($last_heartbeat["time"] > (time() - (60*10)))
		    {
		            $good_heartbeat_scavenger = true;
		    }
		    
	}

	if($good_heartbeat_updater && $good_heartbeat_scavenger)
	{
		echo "The crawler is currently running!";
	}
	else if(!$good_heartbeat_updater)
	{
		echo "The updater appears to be having issues!";
	}	
	else if(!$good_heartbeat_scavenger)
	{
		echo "The scavenger appears to be having issues!";
	}

?>
</div>
</div>
</a>


<div id="maincontent">

<center>
<div class="infonotice">
This page shows basic statistics of REDDcrawl - the backend of REDDbot, which is a software project that attempts to predict rising stories <i>before</i> they reach the front page of <a href="http://www.reddit.com/">Reddit.</a> For more information, contact <a href="http://andrewortman.com">Andrew Ortman</a> at <a href="mailto: andrewortman@gmail.com">andrewortman@gmail.com</a>
</div>
</center>

<h1 class="header_title">Hottest Tracked Stories</h1>

<div class="pagination" style="float:right;	position: relative;	top: 6pt;">
<?php
	print_pagination(get_number_of_stories(), $perpage, $pageno);
?>
</div>
<div style="clear:both; padding-bottom:10px"> </div>
<?php
	$stories = $db->stories->find()->sort(array("last_hotness"=>-1))->skip(($pageno-1)*$perpage)->limit($perpage);
	
	foreach ($stories as $story)
	{
?>
<div class="story_wrapper" id="<?php echo $story['_id'] ?>">
<div class="story">
<?php

	//depending on the last history, use the correct class for the score box 
    $last_history = end($story["history"]);
    if(in_array($story["subreddit"]["name"], $default_subreddits) && $story["last_hotness"] > $fp_min) 
    	echo "<div id='$story[_id]' class='score frontpage'>";
    else 
    	echo "<div id='$story[_id]' class='score'>";
    
    //Echo the score first
	echo number_format($last_history["s"]);
	echo "<br/>";
	//then echo the hotness value for quick debugging
	echo "<div class='hotness'>".number_format($last_history["u"])." 'ups' <br/> " . number_format($last_history["d"]) . " 'downs'</div>";
	
	//End of score box
	echo "</div>";

	if(!empty($story["thumbnail"]) && !$story["nsfw"] && empty($story["self_text"]) && $story["thumbnail"] != "default" && $story["thumbnail"] != "self") 
	echo "<div class='thumbnail'><a href='$story[link]'><img src='$story[thumbnail]'/></a></div>"; ?>
	
	<div class="story_text" >
	<div class="link"> <a href="<?= $story['link']?>"><?php echo trim($story["title"]) ?></a><span class="domain"> (<?php echo $story["domain"] ?>)</span></div>
	<div class = "info">
		r/<?php echo $story["subreddit"]["name"] . " (" . number_format($story["subreddit"]["subscribers"]) . " subscribers)"?>&nbsp;|&nbsp;Comments: <?php echo $last_history["c"]; ?>&nbsp;|&nbsp;Created <?php echo get_time_string($story["created_utc"]); ?>&nbsp;|&nbsp;Last updated <?php echo get_time_string($story["last_update_time"]); ?>
	
		<br/>
		Author: <?php echo $story["author"]["name"]; ?>&nbsp;|&nbsp;Karma: <?php echo $story["author"]["karma_link"]; ?> / <?php echo $story["author"]["karma_comment"]; ?><br/>
		<a href="http://www.reddit.com/comments/<?php  echo $story['_id'] ?>">Reddit Comment Page</a>
	</div>
</div>

</div> <!-- Story -->
<div class="story_details">
	<div class="graph" id="<?php echo $story['_id'] ?>_comment_score_graph"></div>
	<div class="graph" id="<?php echo $story['_id'] ?>_hotness_graph"></div>
</div>
</div> <!-- Story Wrapper -->
<? } ?>

<div style="clear:both"></div>

<center>
<div class="pagination">

<?php
	print_pagination(get_number_of_stories(), $perpage, $pageno);
?>
</div>
</center>

<center>
<div class="copynotice">
This page was created by <a href="http://andrewortman.com">Andrew Ortman</a>. The content however, is created and owned by the users of <a href="http://www.reddit.com">reddit.com&#0153;</a><br/>
Source code for this website can be found under a GPL license at <a href="http://bitbucket.org/andrewortman/reddcrawl">bitbucket</a>
</div>
</center>
</div>
</body>
</html>
