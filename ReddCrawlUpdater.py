'''
Created on Jun 15, 2012

@author: andrewortman
'''
from reddcrawl import Crawler;
from reddcrawl import DatabaseManager;
import logging;
import sys;
import time;
import ConfigParser;

config = ConfigParser.RawConfigParser();
config.read("reddcrawl.conf");

''' Set up logging for reddcrawl module '''
logging.basicConfig(level=logging.INFO, format=('%(filename)s: '  
                                                 '%(levelname)s: '
                                                 '%(funcName)s(): '
                                                 '%(lineno)d:\t'
                                                 '%(message)s')

                                            );
                                            
redd_crawl = Crawler.Crawler(bot_name="reddcrawl-updater by /u/invalidfunction");

try:
    redd_dbm = DatabaseManager.DatabaseManager();
except Exception as e:
    logging.log(logging.CRITICAL, "Could not connect to database! Error: " + str(e));
    sys.exit(1);

while True:
    redd_dbm.UpdateUpdaterHeartbeat();
    
    last_hotness_update = redd_dbm.GetLastFrontpageHotnessUpdate();
    if(last_hotness_update == None or (time.time() - last_hotness_update["time"]) > config.getint("reddcrawl", "hotness-update-interval")):
        #Update the hotness history
        logging.info("Updating the hotness history for the front page.");
        min_hot, max_hot = redd_crawl.GetMinMaxFrontpageHotness();
        if(min_hot == None):
            logging.error("Could not update the hotness history!");
        else:
            logging.info("Adding hotness update: " + str(min_hot) + " -> " + str(max_hot));
            redd_dbm.AddFrontPageHotnessHistory(min_hot, max_hot);
    time_since_last_hotness_update = time.time();
    
    #Keep repeating this operation for at least a minute
    while (time.time() - time_since_last_hotness_update) < config.getint("reddcrawl", "hotness-update-interval"):
        stories_needing_update = redd_dbm.GetStoriesNeedingUpdate(config.getint("reddcrawl", "update-interval"), 100);

        if(len(stories_needing_update) == 0):
            logging.info("No more stories needing updating... moving on");
            time.sleep(5);
            break;
        
        # Get the current karma and comment count of the stories
        while True: #Keep trying on failure
            logging.info("Updating " + str(len(stories_needing_update)) + " story's histories");
            new_histories = redd_crawl.GetStoriesInfo(stories_needing_update[:]);
            if(new_histories == None):
                continue;
            else:
                break;

        for story_id, history in new_histories.iteritems():
            redd_dbm.AddHistoryToStory(story_id, history);
            logging.debug("Updated story '" + story_id + "'");
        
                                                    
