'''
Created on Jun 17, 2012

@author: andrewortman
'''
import math;

class Story(object):
    '''
    classdocs
    '''
    
    _id = "";
    title = "";
    domain = "";
    link = "";
    selftext = "";
    thumbnail = "";
    created_utc = 0;
    nsfw = False;
    author_name = "";
    author_karma_link = 0;
    author_karma_comment = 0;
    subreddit_name = "";
    subreddit_subscribers = 0;
    
    duplicates = 0;
    duplicates_karma_total = 0;
    
    last_update_time = 0;
    last_hotness = 0;
    history = [];
    
    

    def __init__(self, _id, title, domain, link, self_text, thumbnail, created_utc,
                 nsfw, author_name, subreddit_name,  
                 author_karma_link = 0, author_karma_comment = 0, subreddit_subscribers=0,
                 duplicates=0, duplicates_karma_total=0, last_hotness=0, last_update_time=0):
        self._id = _id;
        self.title = title;
        self.domain = domain;
        self.link = link;
        self.self_text = self_text;
        self.thumbnail = thumbnail;
        self.created_utc = created_utc;
        self.nsfw = nsfw;
        self.author_name = author_name;
        self.subreddit_name = subreddit_name;
        self.author_karma_link = author_karma_link;
        self.author_karma_comment = author_karma_comment;
        self.subreddit_subscribers = subreddit_subscribers;
        self.duplicates = duplicates;
        self.duplicates_karma_total = duplicates_karma_total;
        self.last_hotness = last_hotness;
        self.last_update_time = last_update_time;
        
        self.history = [];
        
        
    def setHistory(self, history_dict):
        self.history = history_dict;
    
    def addHistory(self, history_entry):
        self.history.append(history_entry);
        
        #calculate new hotness and set the update time accordingly
        self.last_hotness = calculateHotness(history_entry["s"], self.created_utc);
        self.last_update_time = history_entry["t"];
    
def generateHistory(karma, num_comments, ups, downs, time):
    return {'t': time, 's': karma, "u": ups, "d": downs,  'c': num_comments};

''' Adapted from reddit's source code '''
def calculateHotness(score, creation_time):
    y = 0;
    if score > 0:
        y = 1;
    elif score < 0:
        y = -1;
    
    if abs(score) >= 1:
        z = abs(score);
    else:
        z = 1;
    
    ts = creation_time - 1134028003;
    hotness = math.log10(z) + (y * ts / 45000.0);
    return hotness;
    

