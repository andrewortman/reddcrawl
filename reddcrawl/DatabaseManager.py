'''
Created on Jun 15, 2012

@author: andrewortman
'''

import pymongo;
import logging;
import time;
import Story;

class DatabaseManager(object):
    '''
    Connects to a mongodb database and provides member functions
    for data retrieval and storage
    '''

    mongodb_conn = None;
    mongodb_db = None;
    
    def __init__(self, host='localhost', port=27017, db='reddcrawl'):
        '''
        Connects to the mongodb instance, throws an exception on error
        '''
        self.mongodb_conn = pymongo.Connection(host, port);
        self.mongodb_db = self.mongodb_conn[db];
        
        # Ensure the indices on the database
        logging.info("Connection to database completed. Ensuring indices..");
        self.mongodb_db.stories.ensure_index("last_hotness");
        self.mongodb_db.stories.ensure_index("last_update_time");
        self.mongodb_db.front_page_hotness.ensure_index("time");
        
    
    def GetStory(self, story_id):
        story = self.mongodb_db.stories.find_one({'_id':story_id});
        
        if(story == None):
            return None;
        
        s = Story.Story(story["_id"], story["title"], story["domain"], story["link"],
                  story["self_text"], story["thumbnail"], story["created_utc"], 
                  story["nsfw"], story["author"]["name"], story["subreddit"]["name"],
                  story["author"]["karma_link"], story["author"]["karma_comment"],
                  story["subreddit"]["subscribers"], story["dups"], story["dups_karma"],
                  story["last_hotness"], story["last_update_time"]);
        
        s.setHistory(story["history"]);
        return s;
    
    ''' Adds a brand new story to the database '''
    def AddNewStory(self, story):
        story_dict = {};
        story_dict["_id"] = story._id;
        story_dict["title"] = story.title;
        story_dict["domain"] = story.domain;
        story_dict["link"] = story.link;
        story_dict["self_text"] = story.self_text;
        story_dict["thumbnail"] = story.thumbnail;
        story_dict["created_utc"] = story.created_utc;
        story_dict["nsfw"] = story.nsfw;
        story_dict["dups"] = story.duplicates;
        story_dict["dups_karma"] = story.duplicates_karma_total;
        
        story_dict["author"] = {};
        story_dict["author"]["name"] = story.author_name;
        story_dict["author"]["karma_link"] = story.author_karma_link;
        story_dict["author"]["karma_comment"] = story.author_karma_comment;
        
        story_dict["subreddit"] = {};
        story_dict["subreddit"]["name"] = story.subreddit_name;
        story_dict["subreddit"]["subscribers"] = story.subreddit_subscribers;
        
        story_dict["history"] = story.history;
        
        story_dict["last_hotness"] = story.last_hotness;
        story_dict["last_update_time"] = story.last_update_time;
        self.mongodb_db.stories.insert(story_dict);
    
    ''' Gets a list of stories needing an update '''
    def GetStoriesNeedingUpdate(self, minwaittime=300, limit=25):
        #Stories needing update:
        # 1) Stories that having been updated since minwaittime (in seconds) ago
        # 2) Stories ordered by latest hotness
        stories = self.mongodb_db.stories.find({"last_update_time": {"$lte": time.time()-minwaittime}}).sort("last_hotness", -1).limit(limit);
        
        story_ids = [];
        for story in stories:
            story_ids.append(story["_id"]);
        
        return story_ids; 
    
    def AddHistoryToStory(self,story_id,history):
        created_utc = self.mongodb_db.stories.find_one({"_id": story_id});
        if(created_utc == None):
            logging.critical("Could not grab created_utc of story for new hotness calculation!");
            return;
        
        created_utc = created_utc["created_utc"];
        
        self.mongodb_db.stories.update({"_id": story_id}, {"$set": {"last_update_time": history["t"],
                                                                        "last_hotness": Story.calculateHotness(history["s"], created_utc)},
                                                               "$push":{"history": history}}, safe = True);
    def UpdateScavengerHeartbeat(self):
        self.mongodb_db.scavenger_heartbeat.update({"_id": 0}, {"$set": {"time": time.time()}}, upsert=True);  
        
    def UpdateUpdaterHeartbeat(self):
        self.mongodb_db.updater_heartbeat.update({"_id": 0}, {"$set": {"time": time.time()}}, upsert=True);  

    def AddFrontPageHotnessHistory(self, min_hotness, max_hotness):
        self.mongodb_db.front_page_hotness.insert({"time": time.time(), "min": min_hotness, "max": max_hotness});
    
    def GetLastFrontpageHotnessUpdate(self):
        returned_hotness = self.mongodb_db.front_page_hotness.find().sort("time", -1).limit(1);
        if(returned_hotness.count() == 0):
            return None;
        else:
            return returned_hotness[0];
                  
