'''
Created on Jun 15, 2012

@author: andrewortman
'''

import logging;
import urllib2;
import json;
import time; '''For profiling'''
import math;
import Story;

class Crawler(object):
    '''
    Performs the actual crawling of JSON encoded pages provided
    by the reddit API
    '''

    last_api_call_time = 0;
    
    def __init__(self, reddit_url='http://www.reddit.com/', bot_name='reddcrawl by /u/invalidfunction'):
        '''
        Constructor
        '''
        self.reddit_url = reddit_url;
        self.bot_name = bot_name;
    
    ''' Downloads and parses a reddit page. 
        page_path is the directory path to the page. ie r/all/top
        and parameters is a dictionary of parameters to be added to the URI
        as GET parameters '''
    def ParsePage(self, page_path, parameters={}):
        
        time_since_last_api_call = time.time() - self.last_api_call_time;
        if self.last_api_call_time != 0 and time_since_last_api_call < 2.0:
            time_to_wait = 2.0 - time_since_last_api_call;
            logging.debug("API call too early - sleeping " + str(time_to_wait))
            time.sleep(time_to_wait);
        
        time_start = time.time();
        self.last_api_call_time = time.time();
        
        '''First, build the URL using the specified parameters'''
        url = self.reddit_url + page_path + ".json";
        first = True;
        for key,value in parameters.iteritems():
            if first:
                first = False;
                url = url + "?";
            else:
                url = url + "&";
            url = url + str(key) + "=" + str(value);
        
        time_end_generate = time.time();
        logging.debug("URL generated: " + url + " (took " + \
                    str(round((time_end_generate-time_start) * 1000)) + \
                    " milliseconds)");

        ''' Download the JSON data from reddit '''
        opener = urllib2.build_opener()
        opener.addheaders = [('User-agent', self.bot_name)]
        try:
            usock = opener.open(url, timeout=30)
            data = usock.read()
            usock.close() 
        except urllib2.HTTPError, e:
            logging.error("Server could not handle request! Return code: " + str(e.code));
            return None;
        except Exception, e:
            logging.error("Connection error! " + str(e));
            return None;
        
        time_end_download = time.time();
        logging.debug("Received " + str(len(data)) + " bytes. (took " + \
                    str(round((time_end_download-time_end_generate) * 1000)) + \
                    " milliseconds)");
        '''Decode and Return'''
        try:
            json_decoded = json.loads(data);
        except ValueError, e:
            logging.log(logging.ERROR, "Invalid JSON! Data received: ");
            logging.log(logging.ERROR, data);
            return None;
        
        time_end_parse = time.time();

        logging.debug(json.dumps(json_decoded, sort_keys=True, indent=4));
        
        time_end = time.time();
        logging.debug("Parse complete. (took " + \
                    str(round((time_end_parse-time_end_download) * 1000)) + \
                    " milliseconds)");
        logging.debug("Total download/parse operation took " + \
                    str(round((time_end - time_start) * 1000)) + " milliseconds.");
        
        if("error" in json_decoded):
            logging.error("API returned error: " + str(json_decoded["error"]));
            return None;
        
        return json_decoded;
    
    ''' Gets the duplicate count of a story_id and total karma of each duplicate 
        and returns the information as a tuple (number, karma)  
         On error, returns None and logs error. Should be fault tolerant '''
    def GetDuplicateInfoForStory(self, story_id):
        duplicates_info = self.ParsePage("duplicates/"+str(story_id));
        if(duplicates_info == None):
            logging.error("Could not retrieve duplicates information!");
            return None, None;
        
        duplicate_count = len(duplicates_info[1]["data"]["children"]);
        duplicate_karma_total = 0;
        for child in duplicates_info[1]["data"]["children"]:
            duplicate_karma_total += int(child["data"]["score"]);
        
        return (duplicate_count, duplicate_karma_total);
    
    ''' Returns the link and comment karma respectively as a tuple 
        On error, logs error and returns None. This should be fault-tolerant. '''
    def GetKarmaCountsForUser(self, user_name):
        user_info = self.ParsePage("user/"+user_name+"/about");
        if(user_info == None):
            logging.error("Could not retrieve user information!");
            return None, None;
        
        return (user_info["data"]["link_karma"] , user_info["data"]["comment_karma"])
    
    ''' Returns the number of subscribers to a subreddit
        On error, logs error and returns None. This should be fault-tolerant. '''
    def GetSubredditSubscriberCount(self, subreddit):
        subreddit_info = self.ParsePage("r/" + subreddit + "/about");
        if(subreddit_info == None):
            logging.error("Could not retrieve subreddit information!");
            return None;
        
        return subreddit_info["data"]["subscribers"];
        
    ''' Gets a story listing based off of the subredit, sort, time_range, and limit
        parameters. By default, these parameters are set to list the current hour's
        most popular stories on all subreddits. '''
    def GetStoryListing(self, subreddit="all",sort="top", time_range="hour", count=25, limit=25):   
        if(limit > 100):
            logging.error("Asked for more than 100 stories at once, only doing 100!");
            limit = 100;
        
        last_story_id = "";
        story_listing = {};
        
        total_num_requests = int(math.ceil(count/limit));
        for i in range(0, total_num_requests):
            logging.info("Retrieving chunk " + str(i) + " out of " + str(total_num_requests));
            story_listing_chunk = self.ParsePage("r/" + subreddit + "/top", {"sort": sort, "t": time_range, "limit": limit, "after": last_story_id});
            if(story_listing_chunk == None):
                logging.error("Could not retrieve COMPLETE story listing!");
                return story_listing;
           
            if(len(story_listing_chunk) == 0):
                logging.debug("Found no more stories. breaking");
                break;
             
            for story in story_listing_chunk["data"]["children"]:
                story_listing[story["data"]["id"]] = story["data"];
                
            last_story_id = story_listing_chunk["data"]["after"];
            if(last_story_id == None):
                break;
            
        logging.info("Received a total of " + str(len(story_listing)) + " unique stories in this story listing.");
        #Now create Story objects using data from the API
        to_return = {};
        for story in story_listing.itervalues():
            if story["author"] == "[deleted]":
                logging.info("Deleted author detected, skipping story.");
                continue;
            #Create a basic Story (without any of the extra stuff)
            to_return[story["id"]] = Story.Story(story["id"], story["title"], story["domain"], 
                            story["url"], story["selftext"], story["thumbnail"], 
                            story["created_utc"], story["over_18"], story["author"],
                            story["subreddit"]);
            #Generate the only history entry for this story (since this was not retrieved from the db)
            first_history = Story.generateHistory(story["score"],story["num_comments"], story["ups"], story["downs"], time.time());
            #addHistory will automagically update the hotness and updatetime
            to_return[story["id"]].addHistory(first_history);
            
        return to_return;
    
        
    '''Get history entries for up to 100 story ids'''
    '''Only gets max 100 stories!!'''
    def GetStoriesInfo(self, story_ids):
        
        if(len(story_ids) > 100):
            logging.error("Asked for more than 100 stories, only returning 25!");
        
        ''' Append the t3_ type to the story ids '''
        for i in range(0, len(story_ids)):
            story_ids[i] = "t3_" + story_ids[i];
            
        stories = self.ParsePage("by_id/" + ",".join(story_ids), {"limit": 100});
        if(stories == None):
            logging.error("Could not retrieve individual story updates");
            return None;
        
        to_return = {};
        for story in stories["data"]["children"]:
            to_return[story["data"]["id"]] = Story.generateHistory(story["data"]["score"], 
                                                           story["data"]["num_comments"], story["data"]["ups"], story["data"]["downs"], 
                                                           time.time());
        return to_return;
    
    def GetMinMaxFrontpageHotness(self):
        story_listing = self.ParsePage("/", {"limit": 100, "sort": "hot"}); 
        if(story_listing == None):
            logging.error("Could not request min/max frontpage hotness!");
            return None, None;
        
        first_story = story_listing["data"]["children"][0]["data"];
        last_story = story_listing["data"]["children"][-1]["data"];
        
        top_hotness = Story.calculateHotness(first_story["score"], first_story["created_utc"]);
        bottom_hotness = Story.calculateHotness(last_story["score"], last_story["created_utc"])
        return (bottom_hotness, top_hotness);
